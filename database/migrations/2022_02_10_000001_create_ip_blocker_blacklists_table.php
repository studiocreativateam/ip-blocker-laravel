<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpBlockerBlacklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('ip_blocker_blacklists', static function (Blueprint $table): void {
            $table->id();
            if (DB::table('users')->exists()) {
                $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            } else {
                $table->unsignedBigInteger('user_id')->nullable();
            }
            $table->text('url')->nullable();
            $table->ipAddress('ip_address')->nullable();
            $table->string('user_agent')->nullable();
            $table->dateTime('blocked_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('ip_blocker_blacklists');
    }
}
