<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Models;

use App\Enums\HolidayRequestStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * IpBlocker\Models\IpBlockerBlacklist
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $blocked_at
 * @property-read mixed $user
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereBlockedAt($value)
 * @mixin \Eloquent
 */
class IpBlockerBlacklist extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $casts = [
        'blocked_at' => 'datetime',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(config('ip-blocker.user_model')::class);
    }
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function (self $ipBlockerBlacklist) {
            $ipBlockerBlacklist->blocked_at = now();
        });
    }
}
