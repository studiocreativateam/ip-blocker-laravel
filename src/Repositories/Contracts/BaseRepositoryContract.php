<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryContract
{
    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * @param Model $model
     * @param array $attributes
     * @return bool
     */
    public function update(Model $model, array $attributes): bool;

    /**
     * @param Model $model
     * @return bool
     */
    public function delete(Model $model): bool;

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model
     */
    public function getModel(): Model;

    /**
     * @return Model
     */
    public function getCount(): int;

    /**
     * Find Record based on specific column.
     *
     * @param string $value
     * @param string $column_name
     *
     * @return mixed
     */
    public function getByColumn(string $value, string $column_name = 'id'): ?Model;

    /**
     * @return mixed
     */
    public function query();

    /**
     * Generate drop-down select data with basic IDs.
     *
     * @param string $field_name
     *
     * @return array
     */
    public function getSelectData(string $field_name = 'name');
}
