<?php

return [
    'enabled' => env('IP_BLOCKER_ENABLED', true),

    'database_history' => env('IP_BLOCKER_DATABASE_HISTORY', 30), // days

    'attempts' => env('IP_BLOCKER_ATTEMPTS', 3), // blockade after so many query attempts to one non-existent address

    'how_long' => env('IP_BLOCKER_HOW_LONG', 120), // block for mins.

    'user_model' => App\Models\User::class,

    'whitelist_ips' => [
        '127.0.0.1',
    ],

    'blacklist_ips' => [
//        '127.0.0.1',
    ],
];
