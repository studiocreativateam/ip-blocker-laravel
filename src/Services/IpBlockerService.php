<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Services;

use Illuminate\Http\Request;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerAttemptRepositoryContract;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerBlacklistRepositoryContract;
use StudioCreativaTeam\IpBlockerLaravel\Services\Contracts\IpBlockerServiceContract;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IpBlockerService implements IpBlockerServiceContract
{
    private IpBlockerBlacklistRepositoryContract $ipBlockerBlacklistRepository;
    private IpBlockerAttemptRepositoryContract $ipBlockerAttemptRepository;

    public function __construct(
        IpBlockerBlacklistRepositoryContract $ipBlockerBlacklistRepository,
        IpBlockerAttemptRepositoryContract   $ipBlockerAttemptRepository,
    )
    {
        $this->ipBlockerBlacklistRepository = $ipBlockerBlacklistRepository;
        $this->ipBlockerAttemptRepository = $ipBlockerAttemptRepository;
    }

    public function ipIsBlocked(string $ip = null): bool
    {
        if (empty($ip)) {
            return true;
        }
        if (in_array($ip, config('ip-blocker.whitelist_ips', []))) {
            return false;
        }
        if (in_array($ip, config('ip-blocker.blacklist_ips', []))) {
            return true;
        }
        return $this->ipBlockerBlacklistRepository->isBlockedIP($ip);
    }

    public function blockIP(Request $request): bool
    {
        $ip = $request->ip();

        if ($this->ipIsBlocked($ip)) {
            return true;
        }
        if (in_array($ip, config('ip-blocker.whitelist_ips', []))) {
            return false;
        }

        return (bool)$this->ipBlockerBlacklistRepository->create([
            'ip_address' => $ip,
            'user_agent' => $request->userAgent(),
            'url' => $request->fullUrl(),
            'user_id' => $request->user() ? $request->user()->getKey() : null,
        ]);
    }

    public function render(Request $request, \Throwable $throwable): void
    {
        if ($throwable instanceof NotFoundHttpException) {
            if ($this->ipBlockerAttemptRepository->addAndReturnCountOfAttempts($request->ip(), $request->fullUrl()) > config('ip-blocker.attempts')) {
                if ($this->blockIP($request)) {
                    abort(403);
                }
            }
        }
    }

    public function optimize(): void
    {
        $days = config('ip-blocker.database_history');
        $this->ipBlockerBlacklistRepository->optimize($days);
        $this->ipBlockerAttemptRepository->optimize($days);
    }
}
