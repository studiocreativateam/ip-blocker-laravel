<?php

namespace StudioCreativaTeam\IpBlockerLaravel;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Http\Kernel as HttpKernelInterface;
use Illuminate\Foundation\Application as Laravel;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Laravel\Lumen\Application as Lumen;
use StudioCreativaTeam\IpBlockerLaravel\Console\OptimizeHistoryCommand;
use StudioCreativaTeam\IpBlockerLaravel\Console\PublishCommand;
use StudioCreativaTeam\IpBlockerLaravel\Http\Middleware\CheckIpMiddleware;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\BaseRepository;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\BaseRepositoryContract;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerAttemptRepositoryContract;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerBlacklistRepositoryContract;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\IpBlockerAttemptRepository;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\IpBlockerBlacklistRepository;
use StudioCreativaTeam\IpBlockerLaravel\Services\Contracts\IpBlockerServiceContract;
use StudioCreativaTeam\IpBlockerLaravel\Services\IpBlockerService;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        if ($this->isEnabled()) {
            if ($this->app instanceof Lumen) {
                $this->app->middleware(CheckIpMiddleware::class);
            } elseif ($this->app->bound(HttpKernelInterface::class)) {
                $httpKernel = $this->app->make(HttpKernelInterface::class);
                if ($httpKernel instanceof HttpKernel) {
                    $httpKernel->pushMiddleware(CheckIpMiddleware::class);
                }
            }
            $this->app->booted(function () {
                $schedule = $this->app->make(Schedule::class);
                $schedule->command('ip-blocker:optimize')->daily();
            });
        }

        if ($this->app->runningInConsole()) {
            if ($this->app instanceof Laravel) {
                $this->publishes([
                    __DIR__ . '/../config/' . static::$abstract . '.php' => config_path(static::$abstract . '.php'),
                ], 'config');

                $this->publishes([
                    __DIR__ . '/../database/migrations/' => database_path('migrations'),
                ], 'accountant-migrations');
            }

            $this->registerArtisanCommands();
        }
    }

    public function register(): void
    {
        if ($this->app instanceof Lumen) {
            $this->app->configure(static::$abstract);
        }

        $this->mergeConfigFrom(__DIR__ . '/../config/' . static::$abstract . '.php', static::$abstract);

        $this->app->bind(BaseRepositoryContract::class, BaseRepository::class);
        $this->app->bind(IpBlockerBlacklistRepositoryContract::class, IpBlockerBlacklistRepository::class);
        $this->app->bind(IpBlockerAttemptRepositoryContract::class, IpBlockerAttemptRepository::class);
        $this->app->bind(IpBlockerServiceContract::class, IpBlockerService::class);

        if ($this->isEnabled()) {
            $this->app->alias(IpBlockerServiceContract::class, static::$abstract);
        }
    }

    protected function registerArtisanCommands(): void
    {
        $this->commands([
            PublishCommand::class,
            OptimizeHistoryCommand::class,
        ]);
    }

    public function provides(): array
    {
        return [static::$abstract];
    }
}
