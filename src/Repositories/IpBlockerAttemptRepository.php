<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories;

use StudioCreativaTeam\IpBlockerLaravel\Models\IpBlockerAttempt;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerAttemptRepositoryContract;

class IpBlockerAttemptRepository extends BaseRepository implements IpBlockerAttemptRepositoryContract
{
    public function __construct(IpBlockerAttempt $model)
    {
        parent::__construct($model);
    }

    public function addAndReturnCountOfAttempts(string $ip, string $url): int
    {
        $this->create([
            'ip_address' => $ip,
            'url' => $url,
        ]);

        return $this->model->newQuery()->whereDate('created_at', now())->where('ip_address', $ip)->where('url', $url)->count('id');
    }

    public function optimize(int $days): void
    {
        $this->model->newQuery()->whereDate('created_at', '<', now()->subDays($days))->delete();
    }
}
