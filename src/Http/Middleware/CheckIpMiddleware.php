<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Http\Middleware;

use Closure;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use StudioCreativaTeam\IpBlockerLaravel\Services\Contracts\IpBlockerServiceContract;

class CheckIpMiddleware
{
    private const ADDRESSES = [
        '.env',
        '.*/.env',
        'admincp',
        '_ignition/execute-solution',
        'ipquery',
        'mifs/.;/services/LogService',
        'resolve',
        'query',
        'dns-query',
        'owa/auth/logon.aspx',
        'ecp/Current/exporttool/microsoft.exchange.ediscovery.exporttool.application',
        'Autodiscover/Autodiscover.xml',
        'actuator/health',
        'owa/auth/x.js',
        'c/version.js',
        'stalker_portal/c/version.js',
        'flu/403.html',
        'config.json',
        'idx_config',
        '.git/config',
        '.git/HEAD',
        'api/geojson',
        'api/search',
        'frontend_dev.php',
        'console',
        'bag2',
    ];

    public function handle(Request $request, Closure $next)
    {
        $container = Container::getInstance();

        if ($container->bound(IpBlockerServiceContract::class)) {
            $ipBlockerService = $container->make(IpBlockerServiceContract::class);
            if ($this->isValidGoogleRequest($request->ip(), $request->userAgent())) {
                return $next($request);
            }

            $url = url($request->path());
            foreach (self::ADDRESSES as $path) {
                $pregUrl = str_replace('/', '\/', url($path));
                if (preg_match("/($pregUrl)/i", $url)) {
                    $ipBlockerService->blockIP($request);
                    break;
                }
            }
            if ($ipBlockerService->ipIsBlocked($request->ip())) {
                abort(403, 'Your IP address is temporarily blocked.');
            }
        }

        return $next($request);
    }

    private function isValidGoogleIp($ip): bool
    {
        $hostname = gethostbyaddr($ip);
        return !!preg_match('/\.googlebot|google\.com$/i', $hostname);
    }

    private function isValidGoogleRequest($ip = null, $agent = null): bool
    {
        if (is_null($ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if (is_null($agent)) {
            $agent = $_SERVER['HTTP_USER_AGENT'];
        }

        return strpos($agent, 'Google') !== false && $this->isValidGoogleIp($ip);
    }
}
