<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Services\Contracts;

use Illuminate\Http\Request;

interface IpBlockerServiceContract
{
    public function ipIsBlocked(string $ip = null): bool;

    public function blockIP(Request $request): bool;

    public function render(Request $request, \Throwable $throwable): void;
    
    public function optimize(): void;
}
