<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Models;

use App\Enums\HolidayRequestStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * IpBlocker\Models\IpBlockerBlacklist
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|IpBlockerBlacklist whereCreatedAt($value)
 * @mixin \Eloquent
 */
class IpBlockerAttempt extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function (self $ipBlockerBlacklist) {
            $ipBlockerBlacklist->created_at = now();
        });
    }
}
