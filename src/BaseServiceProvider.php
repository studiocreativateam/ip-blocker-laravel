<?php

namespace StudioCreativaTeam\IpBlockerLaravel;

use Illuminate\Support\ServiceProvider;

abstract class BaseServiceProvider extends ServiceProvider
{
    public static string $abstract = 'ip-blocker';

    protected function isEnabled(): bool
    {
        $config = $this->getUserConfig();
        return $config['enabled'] ?? true;
    }

    protected function getUserConfig(): array
    {
        $config = $this->app['config'][static::$abstract] ?? null;

        return empty($config) ? [] : $config;
    }
}
