# IP Blocker Laravel

Block routes by IP

---

## Requirements
Laravel 7.x and 8.x

---

## Installation

1) To install through composer, run the following command from terminal:

   ```bash
    $ composer require studiocreativateam/ip-blocker-laravel
   ```

2) Publish the config file:

   Run the following command to publish the package config file:

   ```bash
    $ php artisan ip-blocker:publish
    $ php artisan migrate
   ```
3) Enable the interception of attempts to find pages that do not exist to report to IP Blocker by making the following change to your: `App/Exceptions/Handler.php`
   ```bash
    public function render($request, Throwable $e)
    {
        if (app()->bound('ip-blocker')) {
            app('ip-blocker')->render($request, $e);
        }
        return parent::render($request, $e);
    }
   ```
