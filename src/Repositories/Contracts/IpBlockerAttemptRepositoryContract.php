<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts;

interface IpBlockerAttemptRepositoryContract extends BaseRepositoryContract
{
    public function addAndReturnCountOfAttempts(string $ip, string $url): int;

    public function optimize(int $days): void;
}
