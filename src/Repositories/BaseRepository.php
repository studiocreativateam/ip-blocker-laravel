<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories;

use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\BaseRepositoryContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryContract
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param Model $model
     * @param array $attributes
     *
     * @return bool
     */
    public function update(Model $model, array $attributes): bool
    {
        return $model->update($attributes);
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool
    {
        return $model->delete();
    }

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function findWithTrashed(int $id): ?Model
    {
        return $this->model->withTrashed()->find($id);
    }

    public function all(): Collection
    {
        return $this->model->all();
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getCount(): int
    {
        return $this->query()->count();
    }

    public function getByColumn(string $value, string $column_name = 'id'): ?Model
    {
        return $this->query()->where($column_name, $value)->first();
    }

    public function query()
    {
        return call_user_func([$this->model, 'query']);
    }

    public function getSelectData($field_name = 'name')
    {
        $collection = $this->all();

        return call_user_func([$this->model, 'getItems'], $collection, $field_name);
    }
}
