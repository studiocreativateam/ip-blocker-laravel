<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use StudioCreativaTeam\IpBlockerLaravel\Services\Contracts\IpBlockerServiceContract;

class OptimizeHistoryCommand extends Command
{
    protected $name = 'ip-blocker:optimize';

    protected $signature = 'ip-blocker:optimize';

    protected $description = 'Optimize the IP Blocker database.';

    public function handle(): int
    {
        $ipBlockerService = app(IpBlockerServiceContract::class);
        $ipBlockerService->optimize();
        return 0;
    }
}
