<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use StudioCreativaTeam\IpBlockerLaravel\ServiceProvider;

class PublishCommand extends Command
{
    protected $name = 'ip-blocker:publish';

    protected $signature = 'ip-blocker:publish {--attempts=}';

    protected $description = 'Publishes and configures the IP Blocker config.';

    public function handle(): int
    {
        $arg = [];
        $env = [];

        $this->prepareEnvFromCommand($env, $arg, 'attempts', 'IP_BLOCKER_ATTEMPTS');

        $this->info('Publishing IP Blocker config...');
        $this->call('vendor:publish', ['--provider' => ServiceProvider::class]);

        if (!$this->setEnvValues($env)) {
            return 1;
        }

        return 0;
    }

    private function prepareEnvFromCommand(array &$env, array &$arg, string $option, string $envName): bool
    {
        $value = $this->option($option);

        if (!empty($value) || !$this->isEnvKeySet($envName)) {
            if (empty($value)) {
                $valueFromInput = $this->askForInput($option);

                if (empty($valueFromInput)) {
                    $this->error('Please provide a valid ' . $option . ' using the `--' . $option . '` option or setting `' . $envName . '` in your `.env` file!');

                    return false;
                }

                $value = $valueFromInput;
            }

            $env[$envName] = $value;
            $arg['--' . $option] = $value;
        }
        return true;
    }

    private function setEnvValues(array $values): bool
    {
        $envFilePath = app()->environmentFilePath();

        $envFileContents = file_get_contents($envFilePath);

        if (!$envFileContents) {
            $this->error('Could not read `.env` file!');

            return false;
        }

        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {
                if ($this->isEnvKeySet($envKey, $envFileContents)) {
                    $envFileContents = preg_replace("/^{$envKey}=.*?[\s$]/m", "{$envKey}={$envValue}\n", $envFileContents);

                    $this->info("Updated {$envKey} with new value in your `.env` file.");
                } else {
                    $envFileContents .= "{$envKey}={$envValue}\n";

                    $this->info("Added {$envKey} to your `.env` file.");
                }
            }
        }

        if (!file_put_contents($envFilePath, $envFileContents)) {
            $this->error('Updating the `.env` file failed!');

            return false;
        }

        return true;
    }

    private function isEnvKeySet(string $envKey, ?string $envFileContents = null): bool
    {
        $envFileContents = $envFileContents ?? file_get_contents(app()->environmentFilePath());

        return (bool)preg_match("/^{$envKey}=.*?[\s$]/m", $envFileContents);
    }

    private function askForInput(string $option): string
    {
        if ($this->option('no-interaction')) {
            return '';
        }

        while (true) {
            $this->info('');

            $this->question('Please paste the ' . $option . ' here');

            $value = $this->ask($option);

            try {
                return $value;
            } catch (Exception $e) {
                $this->error('The ' . $option . ' is not valid, please make sure to paste a valid ' . $option . '!');
            }
        }
    }
}
