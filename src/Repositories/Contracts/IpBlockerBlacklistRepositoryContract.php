<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts;

interface IpBlockerBlacklistRepositoryContract extends BaseRepositoryContract
{
    public function isBlockedIP(string $ip): bool;
    
    public function optimize(int $days): void;
}
