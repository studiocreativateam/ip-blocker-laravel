<?php

namespace StudioCreativaTeam\IpBlockerLaravel\Repositories;

use Carbon\Carbon;
use StudioCreativaTeam\IpBlockerLaravel\Models\IpBlockerBlacklist;
use StudioCreativaTeam\IpBlockerLaravel\Repositories\Contracts\IpBlockerBlacklistRepositoryContract;

class IpBlockerBlacklistRepository extends BaseRepository implements IpBlockerBlacklistRepositoryContract
{
    public function __construct(IpBlockerBlacklist $model)
    {
        parent::__construct($model);
    }

    public function isBlockedIP(string $ip): bool
    {
        $ipBlockerBlacklists = $this->model->newQuery();
        $howLong = config('ip-blocker.how_long', 15) * 60;
        $instance = Carbon::createFromFormat('Y-m-d H:i:s', now()->format('Y-m-d H:i:s'), 'UTC');
        $instance->setTimezone(config('app.timezone'));
        return $ipBlockerBlacklists->where('ip_address', $ip)->whereRaw("blocked_at + interval '$howLong second' > TO_TIMESTAMP($instance->timestamp)")->exists();
    }
    
    public function optimize(int $days): void
    {
        $this->model->newQuery()->whereDate('blocked_at', '<', now()->subDays($days))->delete();
    }
}
